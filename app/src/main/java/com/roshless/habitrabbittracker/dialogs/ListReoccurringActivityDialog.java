package com.roshless.habitrabbittracker.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.activities.EditReoccurringHabitActivity;
import com.roshless.habitrabbittracker.activities.ReoccurringHabitsListActivity;
import com.roshless.habitrabbittracker.database.ReoccurringHabitDatabaseHandler;

public class ListReoccurringActivityDialog extends Dialog implements View.OnClickListener {

    private Context listContext;
    private Button btnEdit;
    private Button btnDel;
    private int position;
    private TextView textViewHabitName;
    private String habitName;
    private String del_toast_msg_before;
    private String del_toast_msg_after;

    public ListReoccurringActivityDialog(Context context, int position, String habitName) {
        super(context);
        this.listContext = context;
        this.position = position;
        this.habitName = habitName;
        del_toast_msg_before = context.getResources().getString(R.string.toast_msg_habit);
        del_toast_msg_after = context.getResources().getString(R.string.toast_msg_removed);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.operations_dialog);
        btnEdit = (Button) findViewById(R.id.operation_edit_button);
        btnDel = (Button) findViewById(R.id.operation_delete_button);
        textViewHabitName = (TextView) findViewById(R.id.operation_habit_name);
        textViewHabitName.setText(habitName);
        btnEdit.setOnClickListener(this);
        btnDel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        ReoccurringHabitDatabaseHandler dbHandler = new ReoccurringHabitDatabaseHandler(getContext());
        switch (view.getId()) {
            case R.id.operation_edit_button:
                Intent i = new Intent(getContext(), EditReoccurringHabitActivity.class);
                i.putExtra("habit", habitName);
                listContext.startActivity(i);
                ((ReoccurringHabitsListActivity) listContext).finish();
                break;

            case R.id.operation_delete_button:
                if (dbHandler.deleteHabit(dbHandler.findHabit(habitName))) {
                    String message = del_toast_msg_before + " " + habitName + " " + del_toast_msg_after;
                    Toast.makeText(view.getContext(), message, Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(view.getContext(), "Error: Can't find habit",
                            Toast.LENGTH_LONG).show();
                break;
        }
        dismiss();
    }
}
