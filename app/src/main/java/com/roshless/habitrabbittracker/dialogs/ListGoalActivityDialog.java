package com.roshless.habitrabbittracker.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.activities.EditGoalActivity;
import com.roshless.habitrabbittracker.activities.GoalsListActivity;
import com.roshless.habitrabbittracker.database.GoalDatabaseHandler;

public class ListGoalActivityDialog extends Dialog implements View.OnClickListener {

    private Context listContext;
    private Button btnEdit;
    private Button btnDel;
    private int position;
    private TextView textViewGoalName;
    private String goalName;
    private String toastMessageBefore;
    private String toastMessageAfter;

    public ListGoalActivityDialog(Context context, int position, String goalName) {
        super(context);
        this.listContext = context;
        this.position = position;
        this.goalName = goalName;
        toastMessageBefore = context.getResources().getString(R.string.toast_msg_goal);
        toastMessageAfter = context.getResources().getString(R.string.toast_msg_removed);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.operations_dialog);
        btnEdit = (Button) findViewById(R.id.operation_edit_button);
        btnDel = (Button) findViewById(R.id.operation_delete_button);
        textViewGoalName = (TextView) findViewById(R.id.operation_habit_name);
        textViewGoalName.setText(goalName);
        btnEdit.setOnClickListener(this);
        btnDel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        GoalDatabaseHandler dbHandler = new GoalDatabaseHandler(getContext());
        switch (view.getId()) {
            case R.id.operation_edit_button:
                Intent i = new Intent(getContext(), EditGoalActivity.class);
                i.putExtra("goal", goalName);
                listContext.startActivity(i);
                ((GoalsListActivity) listContext).finish();
                break;

            case R.id.operation_delete_button:
                if (dbHandler.deleteHabit(dbHandler.findHabit(goalName))) {
                    String message = toastMessageBefore + " " + goalName + " " + toastMessageAfter;
                    Toast.makeText(view.getContext(), message, Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(view.getContext(), "Error: Can't find goal",
                            Toast.LENGTH_LONG).show();
                break;
        }
        dismiss();
    }
}
