package com.roshless.habitrabbittracker.common;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;

import java.util.List;

public class Habit {

    private int id;

    private String name;
    private String difficulty;
    private LocalTime time;
    private List<Integer> days;
    private LocalDate lastDayDone;

    public Habit() {

    }

    public Habit(String name, String difficulty, LocalTime time, List<Integer> days, LocalDate lastDayDone) {
        this.name = name;
        this.difficulty = difficulty;
        this.time = time;
        this.days = days;
        this.lastDayDone = lastDayDone;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public void setHabitName(String name) {
        this.name = name;
    }

    public void setHabitTime(LocalTime time) {
        this.time = time;
    }

    public void setHabitDays(List<Integer> day) {
        this.days = day;
    }

    public void setLastDayDone(LocalDate lastDayDone) {
        this.lastDayDone = lastDayDone;
    }

    public int getId() {
        return id;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public String getHabitName() {
        return name;
    }

    public LocalTime getHabitTime() {
        return time;
    }

    public List<Integer> getHabitDays() {
        return days;
    }

    public LocalDate getLastDayDone() {
        return lastDayDone;
    }

    @Override
    public String toString() {
        return "\nHabit name= " + name + " difficulty= " + difficulty + " time= " + time;
    }
}
