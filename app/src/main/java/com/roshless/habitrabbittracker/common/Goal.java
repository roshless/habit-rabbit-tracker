package com.roshless.habitrabbittracker.common;

public class Goal {
    private int id;

    private String name;
    private String reoccurringName;
    private int goal;

    public Goal() {
    }

    public Goal(String name, String reoccurringName, int goal) {
        this.name = name;
        this.reoccurringName = reoccurringName;
        this.goal = goal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public String getReoccurringName() {
        return reoccurringName;
    }

    public void setReoccurringName(String reoccurringName) {
        this.reoccurringName = reoccurringName;
    }

    @Override
    public String toString() {
        return "Goal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", reoccurringName=" + reoccurringName +
                ", goal=" + goal +
                '}';
    }
}
