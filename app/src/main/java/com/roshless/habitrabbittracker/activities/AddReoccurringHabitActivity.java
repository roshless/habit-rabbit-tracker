package com.roshless.habitrabbittracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.common.HabitNameLimits;
import com.roshless.habitrabbittracker.common.ReoccurringHabit;
import com.roshless.habitrabbittracker.database.ReoccurringHabitDatabaseHandler;

import org.threeten.bp.LocalDate;

public class AddReoccurringHabitActivity extends AppCompatActivity {
    private TextView name;
    private Spinner spinner;
    ReoccurringHabitDatabaseHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        setContentView(R.layout.new_reoccurring_habit);

        name = (TextView) findViewById(R.id.add_reoccurring_habit_name);
        Button button = (Button) findViewById(R.id.add_reoccurring_habit_button);
        //spinner = (Spinner) findViewById(R.id.add_reoccurring_habit_spinner);
        dbHandler = new ReoccurringHabitDatabaseHandler(this);

        final HabitNameLimits limits = new HabitNameLimits();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limits.isStringNotEmpty(name.getText().toString())) {
                    Toast.makeText(AddReoccurringHabitActivity.this,
                            getString(R.string.no_habit_name_toast_message),
                            Toast.LENGTH_LONG).show();

                } else if (limits.checkForSpecialCharachters(name.getText().toString())) {
                    Toast.makeText(AddReoccurringHabitActivity.this,
                            getString(R.string.wrong_characters_in_string),
                            Toast.LENGTH_LONG).show();

                } else if (dbHandler.habitExists(new ReoccurringHabit(name.getText().toString(),
                        0, LocalDate.now()))) {
                    Toast.makeText(AddReoccurringHabitActivity.this,
                            getString(R.string.already_exists_toast_message), Toast.LENGTH_LONG).show();

                } else {
                    dbHandler.addHabit(new ReoccurringHabit(name.getText().toString(),
                            0, LocalDate.now()));

                    String message = getString(R.string.toast_msg_habit) + " "
                            + name.getText().toString() + " "
                            + getString(R.string.toast_msg_added);
                    Toast.makeText(AddReoccurringHabitActivity.this, message, Toast.LENGTH_LONG).show();
                    dbHandler.close();
                    setResult(RESULT_OK, null);
                    finish();
                }
            }
        });
    }

}
