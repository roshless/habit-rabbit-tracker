package com.roshless.habitrabbittracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.common.Goal;
import com.roshless.habitrabbittracker.common.HabitNameLimits;
import com.roshless.habitrabbittracker.database.GoalDatabaseHandler;
import com.roshless.habitrabbittracker.database.ReoccurringHabitDatabaseHandler;

public class AddGoalActivity extends AppCompatActivity {
    private TextView name;
    private TextView goal;
    private Spinner reoccurring_habit;
    GoalDatabaseHandler dbHandler;
    ReoccurringHabitDatabaseHandler dbHandlerReoccurring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        setContentView(R.layout.new_goal_habit);

        name = (TextView) findViewById(R.id.add_goal_name);
        goal = (TextView) findViewById(R.id.add_goal_value);
        reoccurring_habit = (Spinner) findViewById(R.id.add_goal_spinner);
        Button button = (Button) findViewById(R.id.add_goal_button);

        dbHandler = new GoalDatabaseHandler(this);
        dbHandlerReoccurring = new ReoccurringHabitDatabaseHandler(this);

        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.goal_spinner_reoccurring_habit, dbHandlerReoccurring.getHabitsString());

        spinnerArrayAdapter.setDropDownViewResource(R.layout.goal_spinner_reoccurring_habit);
        reoccurring_habit.setAdapter(spinnerArrayAdapter);

        final HabitNameLimits limits = new HabitNameLimits();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limits.isStringNotEmpty(name.getText().toString()) || limits.isStringNotEmpty(goal.getText().toString())) {
                    Toast.makeText(AddGoalActivity.this,
                            getString(R.string.no_habit_name_toast_message),
                            Toast.LENGTH_LONG).show();

                } else if (limits.checkForSpecialCharachters(name.getText().toString())) {
                    Toast.makeText(AddGoalActivity.this,
                            getString(R.string.wrong_characters_in_string),
                            Toast.LENGTH_LONG).show();

                } else if (dbHandler.habitExists(name.getText().toString())) {
                    Toast.makeText(AddGoalActivity.this,
                            getString(R.string.already_exists_toast_message), Toast.LENGTH_LONG).show();

                } else {
                    dbHandler.addHabit(new Goal(name.getText().toString(),
                            reoccurring_habit.getSelectedItem().toString(), Integer.parseInt(goal.getText().toString())));

                    String message = getString(R.string.toast_msg_habit) + " "
                            + name.getText().toString() + " "
                            + getString(R.string.toast_msg_added);
                    Toast.makeText(AddGoalActivity.this, message, Toast.LENGTH_LONG).show();
                    dbHandler.close();
                    setResult(RESULT_OK, null);
                    finish();
                }
            }
        });
    }
}
