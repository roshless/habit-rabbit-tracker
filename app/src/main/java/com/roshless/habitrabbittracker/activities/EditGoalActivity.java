package com.roshless.habitrabbittracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.common.Goal;
import com.roshless.habitrabbittracker.common.HabitNameLimits;
import com.roshless.habitrabbittracker.database.GoalDatabaseHandler;
import com.roshless.habitrabbittracker.database.ReoccurringHabitDatabaseHandler;

public class EditGoalActivity extends AppCompatActivity {
    private TextView textviewName;
    private TextView textviewGoal;
    private Spinner reoccurring_habit;
    GoalDatabaseHandler dbHandler;
    ReoccurringHabitDatabaseHandler dbHandlerReoccurring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent intent = getIntent();
        setContentView(R.layout.new_goal_habit);

        textviewName = (TextView) findViewById(R.id.add_goal_name);
        textviewGoal = (TextView) findViewById(R.id.add_goal_value);
        reoccurring_habit = (Spinner) findViewById(R.id.add_goal_spinner);
        Button button = (Button) findViewById(R.id.add_goal_button);

        dbHandler = new GoalDatabaseHandler(this);
        dbHandlerReoccurring = new ReoccurringHabitDatabaseHandler(this);

        final Goal goal = dbHandler.getHabit(intent.getStringExtra("goal"));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.goal_spinner_reoccurring_habit, dbHandlerReoccurring.getHabitsString());

        spinnerArrayAdapter.setDropDownViewResource(R.layout.goal_spinner_reoccurring_habit);
        reoccurring_habit.setAdapter(spinnerArrayAdapter);

        button.setText(getString(R.string.edit_habit_apply_changes));
        textviewName.setText(goal.getName());
        textviewGoal.setText(String.valueOf(goal.getGoal()));

        final HabitNameLimits limits = new HabitNameLimits();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (limits.isStringNotEmpty(textviewName.getText().toString()) || limits.isStringNotEmpty(textviewGoal.getText().toString())) {
                    Toast.makeText(EditGoalActivity.this,
                            getString(R.string.no_habit_name_toast_message),
                            Toast.LENGTH_LONG).show();

                } else if (limits.checkForSpecialCharachters(textviewName.getText().toString())) {
                    Toast.makeText(EditGoalActivity.this,
                            getString(R.string.wrong_characters_in_string),
                            Toast.LENGTH_LONG).show();

                } else {
                    dbHandler.updateHabit(goal.getName(), textviewName.getText().toString(),
                            reoccurring_habit.getSelectedItem().toString(), Integer.parseInt(textviewGoal.getText().toString()));

                    String message = getString(R.string.toast_msg_goal) + " "
                            + textviewName.getText().toString() + " "
                            + getString(R.string.edit_toast_msg_after);
                    Toast.makeText(EditGoalActivity.this, message, Toast.LENGTH_LONG).show();
                    dbHandler.close();
                    setResult(RESULT_OK, null);
                    finish();
                    startActivity(new Intent(getApplicationContext(), GoalsListActivity.class));
                }
            }
        });
    }
}
