package com.roshless.habitrabbittracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.adapters.AvatarSetter;
import com.roshless.habitrabbittracker.adapters.HabitAdapter;
import com.roshless.habitrabbittracker.common.Experience;
import com.roshless.habitrabbittracker.database.ExperienceDatabaseHandler;
import com.roshless.habitrabbittracker.database.HabitDatabaseHandler;

import java.text.DecimalFormat;
import java.util.Calendar;

public class ListActivity extends AppCompatActivity {
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_daily_habits);

        ImageView levelAvatar = (ImageView) findViewById(R.id.avatarImageView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ProgressBar progressXP = (ProgressBar) findViewById(R.id.progressBarXP);
        TextView currentLvl = (TextView) findViewById(R.id.textXpLvl);
        TextView currentXP = (TextView) findViewById(R.id.textXpProgress);

        RecyclerView habitRecylerView = (RecyclerView) findViewById(R.id.HabitListView);
        HabitDatabaseHandler dbHandler = new HabitDatabaseHandler(this);
        ExperienceDatabaseHandler dbXPHandler = new ExperienceDatabaseHandler(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        Experience userExperience = dbXPHandler.getUserExperience(1);
        if (userExperience == null) {
            // Add 0 points to database
            dbXPHandler.addUserExperience(new Experience(1, 1, 0.0));
            userExperience = dbXPHandler.getUserExperience(1);
        }

        DecimalFormat df = new DecimalFormat("####0.00");

        double xp = Math.round(userExperience.getExperiencePoints() * 100d) / 100d;
        double level = (double) userExperience.getLevel();

        AvatarSetter avatarSetter = new AvatarSetter((int) level, levelAvatar);
        avatarSetter.setImage();

        double nextLevelTarget = Math.round((0.04 * (Math.pow(level, 3.0)) + 0.8 * (Math.pow(level, 2.0)) + 2.0 * level) * 100d) / 100d;

        progressXP.setProgress((int) (100 * (xp / nextLevelTarget)));
        currentLvl.setText(getString(R.string.progress_bar_text_lvl, String.valueOf((int) level)));
        currentXP.setText(getString(R.string.progress_bar_text_exp, df.format(xp), df.format(nextLevelTarget)));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        habitRecylerView.setLayoutManager(mLayoutManager);

        habitRecylerView.setItemAnimator(new DefaultItemAnimator());

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        RecyclerView.Adapter mAdapter = new HabitAdapter(dbHandler.getHabitsOnDay(day));
        habitRecylerView.setAdapter(mAdapter);

        //close databases
        dbHandler.close();
        dbXPHandler.close();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListActivity.this, AddHabitActivity.class);
                ListActivity.this.startActivityForResult(intent, 1);
            }
        });

        habitRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy < 0 && !fab.isShown())
                    fab.show();
                else if (dy > 0 && fab.isShown())
                    fab.hide();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent refresh = new Intent(this, ListActivity.class);
            startActivity(refresh);
            this.finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_experience) {
            Intent intent = new Intent(ListActivity.this, ExperienceActivity.class);
            ListActivity.this.startActivity(intent);
            return true;
        } else if (id == R.id.action_reoccurring_habits) {
            Intent intent = new Intent(ListActivity.this, ReoccurringHabitsListActivity.class);
            ListActivity.this.startActivity(intent);
            return true;
        } else if (id == R.id.action_goal_habits) {
            Intent intent = new Intent(ListActivity.this, GoalsListActivity.class);
            ListActivity.this.startActivity(intent);
            return true;
        } else if (id == R.id.action_show_all_habits) {
            Intent intent = new Intent(ListActivity.this, ListAllActivity.class);
            ListActivity.this.startActivityForResult(intent, 1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
