package com.roshless.habitrabbittracker.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.adapters.HabitAdapter;
import com.roshless.habitrabbittracker.adapters.ReoccurringHabitAdapter;
import com.roshless.habitrabbittracker.common.Experience;
import com.roshless.habitrabbittracker.common.ReoccurringHabit;
import com.roshless.habitrabbittracker.database.ExperienceDatabaseHandler;
import com.roshless.habitrabbittracker.database.HabitDatabaseHandler;
import com.roshless.habitrabbittracker.database.ReoccurringHabitDatabaseHandler;

import java.text.DecimalFormat;

public class ReoccurringHabitsListActivity extends AppCompatActivity {
    private FloatingActionButton fab;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_all_habits);

        RecyclerView habitRecylerView = (RecyclerView) findViewById(R.id.HabitListView);
        ReoccurringHabitDatabaseHandler dbHandler = new ReoccurringHabitDatabaseHandler(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        habitRecylerView.setLayoutManager(mLayoutManager);

        habitRecylerView.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.Adapter mAdapter = new ReoccurringHabitAdapter(dbHandler.getHabits());
        habitRecylerView.setAdapter(mAdapter);

        dbHandler.close();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReoccurringHabitsListActivity.this, AddReoccurringHabitActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        habitRecylerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy < 0 && !fab.isShown())
                    fab.show();
                else if (dy > 0 && fab.isShown())
                    fab.hide();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent refresh = new Intent(this, ReoccurringHabitsListActivity.class);
            startActivity(refresh);
            this.finish();
        }
    }
}
