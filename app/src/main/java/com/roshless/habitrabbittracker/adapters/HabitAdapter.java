package com.roshless.habitrabbittracker.adapters;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roshless.habitrabbittracker.common.Experience;
import com.roshless.habitrabbittracker.common.Habit;
import com.roshless.habitrabbittracker.activities.ListActivity;
import com.roshless.habitrabbittracker.activities.ListAllActivity;
import com.roshless.habitrabbittracker.dialogs.ListActivityDialog;
import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.database.ExperienceDatabaseHandler;
import com.roshless.habitrabbittracker.database.HabitDatabaseHandler;

import org.threeten.bp.LocalDate;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

public class HabitAdapter extends RecyclerView.Adapter<HabitAdapter.HabitViewHolder> {
    private List<Habit> habitList;
    private String[] strDays = new String[]{"placeholder", "Sunday", "Monday", "Tuesday", "Wednesday", "Thurseday", "Friday", "Saturday"};

    public HabitAdapter(List<Habit> habitList) {
        this.habitList = habitList;
    }

    @Override
    public HabitAdapter.HabitViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        View habitView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.element_on_habit_list, parent, false);
        return new HabitViewHolder(habitView);
    }

    @Override
    public void onBindViewHolder(@NonNull HabitViewHolder habitViewHolder, int position) {
        String daysString = "";
        for (Integer day : habitList.get(position).getHabitDays()) {
            daysString = daysString.concat(strDays[day] + ", ");
        }
        daysString = daysString.substring(0, daysString.length() - 2);
        String description = habitList.get(position).getDifficulty() + ", " +
                habitList.get(position).getHabitTime() + " on " +
                daysString;

        habitViewHolder.habitName.setText(habitList.get(position).getHabitName());
        habitViewHolder.habitDescription.setText(description);

        if (habitViewHolder.habitCheckbox.getVisibility() == View.GONE) {
            HabitDatabaseHandler dbHandler = new HabitDatabaseHandler(habitViewHolder.itemView.getContext());

            if (dbHandler.getHabit(habitViewHolder.habitName.getText().toString()).getLastDayDone() != null
                    && dbHandler.isHabitToday(habitViewHolder.habitName.getText().toString())) {
                // Disable listener for this change
                habitViewHolder.habitCheckbox.setOnCheckedChangeListener(null);

                habitViewHolder.habitCheckbox.setChecked(true);
                habitViewHolder.habitCheckbox.setVisibility(View.VISIBLE);

                // Enable it again
                habitViewHolder.setListener();
            }
        }
    }

    @Override
    public int getItemCount() {
        return habitList.size();
    }

    public class HabitViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView habitName;
        public TextView habitDescription;
        public CheckBox habitCheckbox;
        private CompoundButton.OnCheckedChangeListener onCheckedListener;

        public HabitViewHolder(final View view) {
            super(view);
            habitName = (TextView) view.findViewById(R.id.habit_name);
            habitDescription = (TextView) view.findViewById(R.id.habit_description);
            habitCheckbox = (CheckBox) view.findViewById(R.id.habit_checkbox);

            if (view.getContext() instanceof ListAllActivity) {
                habitCheckbox.setVisibility(View.GONE);
            }

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View view) {
                    ListActivityDialog dialog = new ListActivityDialog(view.getContext(),
                            getAdapterPosition(), habitName.getText().toString());
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            // Worst idea I had in a long time but at least it works.
                            HabitDatabaseHandler dbHandler = new HabitDatabaseHandler(view.getContext());
                            activityCheck(view, dbHandler);
                        }
                    });
                    return true;
                }
            });

            onCheckedListener = new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    ExperienceDatabaseHandler dbXPHandler = new ExperienceDatabaseHandler(view.getContext());
                    HabitDatabaseHandler dbHabitHandler = new HabitDatabaseHandler(view.getContext());

                    Experience userExperience = dbXPHandler.getUserExperience(1);
                    Habit thisHabit = dbHabitHandler.getHabit(habitName.getText().toString());

                    if (view.getContext() instanceof ListActivity) {
                        if (userExperience == null) {
                            // Add 0 points to database
                            dbXPHandler.addUserExperience(new Experience(1, 1, 0.0));
                            userExperience = dbXPHandler.getUserExperience(1);
                        }
                        // Update XP points and level
                        userExperience.addExperience(thisHabit.getDifficulty());

                        dbXPHandler.updateUserExperience(1, userExperience.getLevel(), userExperience.getExperiencePoints());

                        // set this habit's status to done
                        dbHabitHandler.updateHabit(thisHabit.getHabitName(), thisHabit.getHabitName(),
                                thisHabit.getDifficulty(), thisHabit.getHabitTime(),
                                thisHabit.getHabitDays(), LocalDate.now());
                        activityCheck(view, dbHabitHandler);
                        compoundButton.setChecked(false);

                        ProgressBar progressXP = (ProgressBar) ((ListActivity) view.getContext()).findViewById(R.id.progressBarXP);
                        TextView currentLvl = (TextView) ((ListActivity) view.getContext()).findViewById(R.id.textXpLvl);
                        TextView currentXP = (TextView) ((ListActivity) view.getContext()).findViewById(R.id.textXpProgress);
                        ImageView avatarImageView = (ImageView) ((ListActivity) view.getContext()).findViewById(R.id.avatarImageView);

                        AvatarSetter avatarSetter = new AvatarSetter((int) userExperience.getLevel(), avatarImageView);
                        avatarSetter.setImage();

                        DecimalFormat df = new DecimalFormat("####0.00");
                        progressXP.setProgress(userExperience.getProgressBar());
                        currentLvl.setText(view.getResources().getString(R.string.progress_bar_text_lvl, String.valueOf((int) userExperience.getLevel())));
                        currentXP.setText(view.getResources().getString(R.string.progress_bar_text_exp, df.format(userExperience.getLevel()), df.format(userExperience.getNextLevelTarget())));

                    } else if (view.getContext() instanceof ListAllActivity) {
                        userExperience.subExperience(thisHabit.getDifficulty());
                        dbXPHandler.updateUserExperience(1, userExperience.getLevel(), userExperience.getExperiencePoints());

                        dbHabitHandler.updateHabit(thisHabit.getHabitName(), thisHabit.getHabitName(),
                                thisHabit.getDifficulty(), thisHabit.getHabitTime(),
                                thisHabit.getHabitDays(), null);
                    }
                    dbHabitHandler.close();
                    dbXPHandler.close();
                }
            };

            setListener();
        }

        private void activityCheck(View view, HabitDatabaseHandler dbHandler) {
            // You have no idea how long I looked for this.
            if (view.getContext() instanceof ListAllActivity) {
                habitList = dbHandler.getHabits();
            } else {
                Calendar calendar = Calendar.getInstance();
                int day = calendar.get(Calendar.DAY_OF_WEEK);

                habitList = dbHandler.getHabitsOnDay(day);
            }
            notifyDataSetChanged();
        }

        public void setListener() {
            habitCheckbox.setOnCheckedChangeListener(onCheckedListener);
        }
    }
}
