package com.roshless.habitrabbittracker.adapters;

import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.roshless.habitrabbittracker.R;
import com.roshless.habitrabbittracker.common.ReoccurringHabit;
import com.roshless.habitrabbittracker.database.ReoccurringHabitDatabaseHandler;
import com.roshless.habitrabbittracker.dialogs.ListReoccurringActivityDialog;

import java.util.List;

public class ReoccurringHabitAdapter extends RecyclerView.Adapter<ReoccurringHabitAdapter.ReoccurringHabitViewHolder> {
    private List<ReoccurringHabit> habitList;

    public ReoccurringHabitAdapter(List<ReoccurringHabit> habitList) {
        this.habitList = habitList;
    }

    @Override
    public ReoccurringHabitAdapter.ReoccurringHabitViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                 int viewType) {
        View habitView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.element_on_reoccurring_habit_list, parent, false);
        return new ReoccurringHabitViewHolder(habitView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReoccurringHabitViewHolder habitViewHolder, int position) {
        String description = String.valueOf(habitList.get(position).getCounter());

        habitViewHolder.habitName.setText(habitList.get(position).getHabitName());
        habitViewHolder.habitDescription.setText(description);
    }

    @Override
    public int getItemCount() {
        return habitList.size();
    }

    public class ReoccurringHabitViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView habitName;
        public TextView habitDescription;
        public Button habitDonePlus;
        public Button habitDoneMinus;

        public ReoccurringHabitViewHolder(final View view) {
            super(view);
            habitName = (TextView) view.findViewById(R.id.reoccurring_habit_name);
            habitDescription = (TextView) view.findViewById(R.id.reoccurring_habit_counter);
            habitDonePlus = (Button) view.findViewById(R.id.reoccurring_habit_button_increase);
            habitDoneMinus = (Button) view.findViewById(R.id.reoccurring_habit_button_decrease);

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View view) {
                    ListReoccurringActivityDialog dialog = new ListReoccurringActivityDialog(view.getContext(),
                            getAdapterPosition(), habitName.getText().toString());
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            // Worst idea I had in a long time but at least it works.
                            ReoccurringHabitDatabaseHandler dbHandler = new ReoccurringHabitDatabaseHandler(view.getContext());
                            habitList = dbHandler.getHabits();
                            notifyDataSetChanged();
                        }
                    });
                    return true;
                }
            });


            habitDonePlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ReoccurringHabitDatabaseHandler dbHandler = new ReoccurringHabitDatabaseHandler(view.getContext());
                    dbHandler.increaseCounter(habitName.getText().toString());
                    dbHandler.close();

                    Integer counter = Integer.valueOf(habitDescription.getText().toString()) + 1;
                    habitDescription.setText(String.valueOf(counter));
                }
            });

            habitDoneMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ReoccurringHabitDatabaseHandler dbHandler = new ReoccurringHabitDatabaseHandler(view.getContext());
                    dbHandler.decreaseCounter(habitName.getText().toString());
                    dbHandler.close();

                    Integer counter = Integer.valueOf(habitDescription.getText().toString()) - 1;
                    habitDescription.setText(String.valueOf(counter));
                }
            });
        }
    }
}
