package com.roshless.habitrabbittracker;

import com.roshless.habitrabbittracker.common.Habit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalTime;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HabitTest {

    private int id = 1;

    private String name = "test";
    private String difficulty = "easy";
    private LocalTime time = LocalTime.of(12, 50);
    private List<Integer> days = Arrays.asList(1, 2, 3);
    private LocalDate lastDayDone = LocalDate.of(1994, 01, 01);

    @Mock
    private Habit habit;

    @Before
    public void beforeEachTest() {
        habit = new Habit(name, difficulty, time, days, lastDayDone);
        habit.setId(id);
    }

    @Test
    public void getName_test() {
        assertTrue(habit.getHabitName().equals(name));
    }

    @Test
    public void getDifficulty_test() {
        assertTrue(habit.getDifficulty().equals(difficulty));
    }

    @Test
    public void getTime_test() {
        assertTrue(habit.getHabitTime().equals(time));
    }

    @Test
    public void getDays_test() {
        assertTrue(habit.getHabitDays().equals(days));
    }

    @Test
    public void getLastDayDone_test() {
        assertTrue(habit.getLastDayDone().equals(lastDayDone));
    }

    @Test
    public void getId_test() {
        assertTrue(habit.getId() == id);
    }

    @Test
    public void setName_test() {
        String temp = "test2";
        habit.setHabitName(temp);
        assertTrue(habit.getHabitName().equals(temp));
    }

    @Test
    public void setDifficulty_test() {
        String temp = "Hard";
        habit.setDifficulty(temp);
        assertTrue(habit.getDifficulty().equals(temp));
    }

    @Test
    public void setTime_test() {
        LocalTime temp = LocalTime.now();
        habit.setHabitTime(temp);
        assertTrue(habit.getHabitTime().equals(temp));
    }

    @Test
    public void setDays_test() {
        List<Integer> temp = Arrays.asList(5, 4, 6);
        habit.setHabitDays(temp);
        assertTrue(habit.getHabitDays().equals(temp));
    }

    @Test
    public void setLastDayDone_test() {
        LocalDate temp = LocalDate.of(2010, 01, 01);
        habit.setLastDayDone(temp);
        assertTrue(habit.getLastDayDone().equals(temp));
    }

    @Test
    public void setId_test() {
        int temp = 100;
        habit.setId(temp);
        assertTrue(habit.getId() == temp);
    }

    @Test
    public void toString_test() {
        String temp = "\nHabit name= " + name + " difficulty= " + difficulty + " time= " + time;
        assertTrue(habit.toString().equals(temp));
    }
}
