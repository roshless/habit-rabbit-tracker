package com.roshless.habitrabbittracker;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.roshless.habitrabbittracker.activities.ReoccurringHabitsListActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class EditReoccurringHabitTest {
    private String stringToBetyped;

    @Rule
    public ActivityTestRule<ReoccurringHabitsListActivity> activityTestRule = new ActivityTestRule<>(ReoccurringHabitsListActivity.class);

    @Before
    public void initValidString() {
        stringToBetyped = "testName";
    }
    @Test
    public void editReoHabitShortName() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_reoccurring_habit_name)).perform(clearText()).perform(typeText("z"), closeSoftKeyboard());;
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
        //onView(withText("APPLY CHANGES")).perform(click());
    }
    @Test
    public void editReoHabitLongName() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(1,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_reoccurring_habit_name)).perform(clearText()).perform(typeText("zssjdhvbjsdavhbhjdsavkjhbdsjsdfagsd"), closeSoftKeyboard());;
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
        //onView(withText("APPLY CHANGES")).perform(click());
    }
    @Test
    public void editReoHabitSpecChar() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(2,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_reoccurring_habit_name)).perform(clearText()).perform(typeText("#$^**@1"), closeSoftKeyboard());;
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
        //onView(withText("APPLY CHANGES")).perform(click());
    }
    @Test
    public void editReoHabitNumbersAndLetters() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(3,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_reoccurring_habit_name)).perform(clearText()).perform(typeText("1355Ubgdcb"), closeSoftKeyboard());;
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
        //onView(withText("APPLY CHANGES")).perform(click());
    }
    @Test
    public void editReoHabitWithSpace() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(4,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_reoccurring_habit_name)).perform(clearText()).perform(typeText("nice name"), closeSoftKeyboard());;
        onView(withId(R.id.add_reoccurring_habit_button)).perform(click());
        //onView(withText("APPLY CHANGES")).perform(click());
    }

}
