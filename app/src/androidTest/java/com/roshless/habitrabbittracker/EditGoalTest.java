package com.roshless.habitrabbittracker;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.Spinner;

import com.roshless.habitrabbittracker.activities.GoalsListActivity;
import com.roshless.habitrabbittracker.activities.ReoccurringHabitsListActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class EditGoalTest {

    private String stringToBetyped;

    @Rule
    public ActivityTestRule<GoalsListActivity> activityTestRule = new ActivityTestRule<>(GoalsListActivity.class);

    @Before
    public void initValidString() {
        stringToBetyped = "test name";
    }
    @Test
    public void editGoalNameWithSpace() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_goal_name)).perform(clearText()).perform(typeText("edit name "), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(clearText()).perform(typeText("18"), closeSoftKeyboard());
        onView(withText("APPLY CHANGES")).perform(click());
    }
    @Test
    public void editGoalLongName() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(1,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_goal_name)).perform(clearText()).perform(typeText("dfjgnkdjfgdn kfd gdggggfd "), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(clearText()).perform(typeText("23"), closeSoftKeyboard());
        onView(withText("APPLY CHANGES")).perform(click());
    }
    @Test
    public void editGoalNumbersAndLetters() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(2,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_goal_name)).perform(clearText()).perform(typeText("Letters and 76539"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(clearText()).perform(typeText("3"), closeSoftKeyboard());
        onView(withText("APPLY CHANGES")).perform(click());
    }
    @Test
    public void editGoalSpecChar() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(3,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_goal_name)).perform(clearText()).perform(typeText("$%^&@ $"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(clearText()).perform(typeText("3"), closeSoftKeyboard());
        onView(withText("APPLY CHANGES")).perform(click());
    }
    @Test
    public void editGoalBigValue() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(4,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.add_goal_name)).perform(clearText()).perform(typeText("big value edit"), closeSoftKeyboard());
        onView(withId(R.id.add_goal_value)).perform(clearText()).perform(typeText("354215448"), closeSoftKeyboard());
        onView(withText("APPLY CHANGES")).perform(click());
    }
}
