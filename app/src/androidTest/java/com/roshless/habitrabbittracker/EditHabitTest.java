package com.roshless.habitrabbittracker;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;

import com.roshless.habitrabbittracker.activities.EditHabitActivity;
import com.roshless.habitrabbittracker.activities.ListActivity;
import com.roshless.habitrabbittracker.common.Habit;
import com.roshless.habitrabbittracker.dialogs.ListActivityDialog;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class EditHabitTest {

    private String stringToBetyped;

    @Rule
    public ActivityTestRule<ListActivity> activityTestRule = new ActivityTestRule<>(ListActivity.class);

    @Before
    public void initValidString() {
        stringToBetyped ="testName";
    }

    @Test
    public void editHabitEasy() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.habitNameTextView)).perform(clearText()).perform(typeText("habit easy with edit"), closeSoftKeyboard());;
        onView(withId(R.id.radioButtonEasy)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }


    @Test
    public void editHabitMedium() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(1,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.habitNameTextView)).perform(clearText()).perform(typeText("medium with edit"), closeSoftKeyboard());;
        onView(withId(R.id.radioButtonMedium)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void editHabitHard() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(2,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.habitNameTextView)).perform(clearText()).perform(typeText("hard habit with edit"), closeSoftKeyboard());;
        onView(withId(R.id.radioButtonHard)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void editHabitWithAlarm() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(3,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.habitNameTextView)).perform(clearText()).perform(typeText("add alarm edit"), closeSoftKeyboard());;
        onView(withId(R.id.radioButtonMedium)).perform(click());
        onView(withId(R.id.switch1)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void editHabitWithSpecChar() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(4,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.habitNameTextView)).perform(clearText()).perform(typeText("$%&(#//;"), closeSoftKeyboard());;
        onView(withId(R.id.radioButtonHard)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
    @Test
    public void editHabitShortName() {
        onView(withId(R.id.HabitListView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(4,longClick()));
        onView(withText("Edit")).perform(click());
        onView(withId(R.id.habitNameTextView)).perform(clearText()).perform(typeText("ab"), closeSoftKeyboard());;
        onView(withId(R.id.radioButtonHard)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }
}
